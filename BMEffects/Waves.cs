﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BMEffects
{
    public static class Waves
    {
        /// <summary>
        /// Generates a wave with a tail that "chases" down the strip
        /// </summary>
        /// <param name="stripLength">Number of points to generate (length of LED strip).</param>
        /// <param name="shift">Increase this to progress the wave forward.</param>
        /// <param name="k">Sharpening/curving factor - start about 2.0 and tweak from there.</param>
        /// <param name="leadfactor">Bias towards the leading edge - 5-15ish work nicely.</param>
        /// <returns></returns>
        public static IEnumerable<double> Chase(uint stripLength, double shift, double k, double leadfactor)
        {
            double trail(double x) => Math.Pow(x / (stripLength * (1 - 1 / leadfactor)), 1.5);
            double lead(double x) => (stripLength - x) * (leadfactor / stripLength);
            double wave(double x) => Math.Min(lead(x), trail(x));

            return Enumerable.Range(0, (int)stripLength).Select(x => Math.Pow(wave((x - shift).Mod(stripLength)), k));
        }

        public static IEnumerable<double> Flatline(uint stripLength, double val) => Enumerable.Range(0, (int)stripLength).Select(i => val);

    }
}
