﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMEffects
{
    public static class MathUtils
    {
        /// <summary>
        /// Sin, but 0..1 instead of -1..1
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static double SuperSin(double a) => Math.Sin(a) * 0.5 + 0.5;

        public static double HueSwing(double center, double swing, double shift) => (center + swing * Math.Sin(shift)).Mod(360);

        public static T Clamp<T>(this T val, T min, T max) where T : IComparable<T>
        {
            if (val.CompareTo(min) < 0) return min;
            else if (val.CompareTo(max) > 0) return max;
            else return val;
        }
    }
}
