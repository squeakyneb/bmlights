﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMEffects
{
    public static class BMExtensions
    {
        public static double Mod(this double x, double y) => ((x % y) + y) % y;
    }
}
