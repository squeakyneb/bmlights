﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenRGB.NET.Models;

namespace BMEffects
{
    public static class ColorExtensions
    {
        private static byte Safescale(byte x, double f) => (byte)Math.Min(Math.Max(((double)x) * f, 0), 255);

        public static Color Scale(this Color c, double s)
        {

            return new Color(Safescale(c.R, s), Safescale(c.G, s), Safescale(c.B, s));
        }

        public static IEnumerable<Color> Colorise(this IEnumerable<double> wave, Color c)
        {
            return wave.Select(x => c.Scale(x));
        }

        public static IEnumerable<double> Add(this IEnumerable<double> a, double b)
        {
            return a.Select(x => x + b);
        }

        public static IEnumerable<double> Scale(this IEnumerable<double> a, double b)
        {
            return a.Select(x => x * b);
        }

        public static IEnumerable<double> Stretch(this IEnumerable<double> a, double min, double max)
        {
            double range = max - min;

            return a.Select(x => x * range + min);
        }
    }
}