﻿using OpenRGB.NET.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMEffects;

namespace SimpleLights
{
    public class KeyboardPlasma : IAnimator
    {
        Zone zone;
        List<(double, double)> positionMap;

        public KeyboardPlasma(Zone _zone)
        {
            if (_zone.MatrixMap == null)
            {
                throw new Exception("Can't do a matrix animation for a device with a null map!");
            }
            zone = _zone;

            positionMap = Enumerable.Range(0, (int)zone.LedCount).Select(i =>
            {
                foreach (var x in Enumerable.Range(0, (int)zone.MatrixMap.Width))
                {
                    foreach (var y in Enumerable.Range(0, (int)zone.MatrixMap.Height))
                    {
                        if (zone.MatrixMap.Matrix[y, x] == i)
                        {
                            return ((double)x, (double)y);
                        }
                    }

                }
                return (-100, -100);
            }).ToList();
        }

        Color plasmify((double, double) p, AnimUpdateState _state)
        {
            double x = p.Item1;
            double y = p.Item2;

            // 0.2 gives a nice baseline minimum brightness for full keyboard illumination in dim lighting
            // Mixing T into the Y and Z gives some upwards movement but also evolution that stops it feeling too "stiff"
            // Different scaling on X and Y feels right given the larger horizontal resolution - shapes are distinct but not too small and noisy in each direction
            double lumaN = (0.3 + (double)Perlin.Noise((float)(x * 0.2), (float)(y * 0.3 + _state.T * 0.7), (float)(_state.T * .3))).Clamp(0.2, 1.0);
            double hueN = (double)Perlin.Noise((float)(x * 0.1), (float)(y * 0.1), (float)(_state.T * 1.0));

            return Color.FromHsv(hueN * 45 - 10, 1.0, lumaN);
        }

        public void Update(AnimUpdateState _state)
        {
            zone.Update(Enumerable.Range(0, (int)zone.LedCount)
                .Select(i => positionMap[i])
                .Select(p => plasmify(p, _state))
                .ToArray()
                );
        }
    }
}
