﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;

using OpenRGB.NET;
using OpenRGB.NET.Enums;
using OpenRGB.NET.Models;

using BMEffects;
using Microsoft.Win32;

namespace SimpleLights
{
    class Program
    {

        public static NotifyIcon trayIcon;
        private static bool RunningLights = true;
        private static bool RunningApplication = true;
        private static bool HoldingOnError = false;
        private static bool SessionLocked = false;

        enum RunLightsResult
        {
            Done,
            NoDevices
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Starting...");

            if (Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location)).Count() > 1)
            {
                MessageBox.Show("Another instance of this process appears to be running already", "BMLights",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            StartForms();
            if (args.Contains("--slowstart"))
            {
                trayIcon.Text = "BMLights slow-starting...";
                Thread.Sleep(20 * 1000);
            }

            do
            {
                while (HoldingOnError) { Thread.Sleep(100); }
                trayIcon.Text = "BMLights looking for OpenRGB...";
                try
                {
                    OpenRGBClient client = new OpenRGBClient(name: "OpenRGB.NET BMLights");
                    var result = RunLights(client);
                    if (result == RunLightsResult.NoDevices)
                    {
                        trayIcon.Text = "BMLights found OpenRGB but no devices - check OpenRGB";
                        Thread.Sleep(3000);
                    }
                }
                catch (System.TimeoutException) // OpenRGB isn't running
                {
                    Thread.Sleep(3000);
                }
                catch (System.Net.Sockets.SocketException) // OpenRGB quit?
                {
                    Thread.Sleep(3000);
                }
                catch (System.IO.IOException ex)
                {
                    if (ex.Message == "Length of header was zero")
                    {
                        // Idk man this just happens sometimes during OpenRGB startup
                        Thread.Sleep(3000);
                    }
                    else
                    {
                        throw ex;
                    }
                }
                catch (Exception ex)
                {
#if !DEBUG
                    MessageBox.Show(ex.ToString(), "BMLights had an oops");
                    trayIcon.Text = "BMLights holding on error - reload to try again";
                    HoldingOnError = true;
#else
                    throw ex;
#endif

                }
            } while (RunningApplication);
        }
        static RunLightsResult RunLights(OpenRGBClient client)
        {
            Device[] devices = client.GetAllControllerData().
                Where(dev => dev.Modes.Select(mode => mode.Name).Contains("Direct")).ToArray();
            if (devices.Length == 0)
            {
                Console.WriteLine("No devices :(");
                return RunLightsResult.NoDevices;
            }

            foreach (var d in devices)
            {
                int modeId = d.Modes.Select((m, i) => (m, i)).First(t => t.m.Name == "Direct").i;
                client.SetMode(d.ID, modeId);
                Thread.Sleep(100);
            }

            IEnumerable<Zone> zones = devices.
                SelectMany(dev => dev.Zones).
                Where(z => z.Name != "GPU Zone");
            Console.WriteLine("Zones: ");
            foreach (var z in zones)
            {
                Console.WriteLine("\t{0}, {1}", z.Name, z.Type);
            }

            IEnumerable<Zone> matrixZones = zones.Where(z => z.Type == ZoneType.Matrix);
            Console.WriteLine("Matrix!");
            foreach (var z in matrixZones)
            {
                Console.WriteLine("\t{0}, {1}, {2}, mapped={3}", z.Name, z.Type, z.LedCount, z.MatrixMap != null);
            }
            List<IAnimator> animators = matrixZones.Select(z => (IAnimator)new KeyboardPlasma(z)).ToList();
            var addressableZones = zones.Where(z => z.Type == ZoneType.Linear).ToList();
            var singleZones = zones.Where(z => z.Type == ZoneType.Single).ToList();

            double targetFPS = 30;
            double targetInterval = ((1.0 / targetFPS) * 1000);

            Stopwatch clock = Stopwatch.StartNew();

            RunningLights = true;
            while (RunningLights)
            {

                trayIcon.Text = "BMLights Running...";

                double startTime = clock.ElapsedMilliseconds;
                AnimUpdateState animUpdateState = new AnimUpdateState();
                double T = startTime / 1000;
                double lockFactor = SessionLocked ? 0.15 : 1.0;
                animUpdateState.T = T;
                animUpdateState.locked = SessionLocked;

                foreach (Zone z in addressableZones)
                {
                    if (z.LedCount == 53) // 9 x 3 Thermaltake fans + 26 Coolermaster GPU support
                    {
                        var c = Color.FromHsv(MathUtils.HueSwing(354, -20, T / 7.0), 1.0, lockFactor);
                        var gpuC = Color.FromHsv(MathUtils.HueSwing(354, -20, T / 4.0), 1.0, lockFactor);
                        var fan = Waves.Chase(9, 3 * T, 1.5, 8.0).Stretch(-.08, 1.0).Colorise(c);
                        var gpuSupport = Waves.Chase(26, 3 * T, 2.0, 12.0).Stretch(-.1, 1.0).Colorise(gpuC);
                        var fullstrip = fan.Concat(fan).Concat(fan).Concat(gpuSupport);
                        z.Update(fullstrip.ToArray());
                    }
                    else if (z.LedCount == 12) // Corsair RAM
                    {
                        var c = Color.FromHsv(MathUtils.HueSwing(354, -20, T / 3.0 + z.DeviceID), 1.0, lockFactor);
                        var strip = Waves.Chase(12, 4.0 * T + z.DeviceID * 3.0, 2.0, 8.0).Stretch(-.3, 1.0).Colorise(c);
                        z.Update(strip.ToArray());
                    }
                    else if (z.LedCount == 40) // Corsair strips
                    {
                        var c = Color.FromHsv(MathUtils.HueSwing(354, -20, T / 3.0), 1.0, lockFactor);
                        var strip = Waves.Chase(20, 3 * T, 2.5, 6.0).Stretch(-.2, 1.0).Colorise(c);
                        var fullstrip = strip.Concat(strip);
                        z.Update(fullstrip.ToArray());
                    }
                    else if (z.LedCount == 24) // Arctic AIO
                    {
                        var c1 = Color.FromHsv(MathUtils.HueSwing(354, -20, T / 5.0), 1.0, lockFactor);
                        var c2 = Color.FromHsv(MathUtils.HueSwing(354, -20, T / 5.0 + Math.PI * (2.0f / 3.0f)), 1.0, lockFactor);
                        var fan1 = Waves.Chase(12, 3 * T, 1.5, 8.0).Stretch(-.08, 1.0).Colorise(c1);
                        var fan2 = Waves.Chase(12, 3 * T, 1.5, 8.0).Stretch(-.08, 1.0).Colorise(c2);
                        var fullstrip = fan1
                       .Concat(fan2);
                        z.Update(fullstrip.ToArray());
                    }
                    else if (z.LedCount == 5) // ASUS mobo
                    {
                        Color colourswing(int i) => Color.FromHsv(MathUtils.HueSwing(354, -20, T + i), 1.0, lockFactor);
                        var boardc = Enumerable.Range(0, 3).Select(i => colourswing(i));
                        var stripA = new Color[] { Color.FromHsv(MathUtils.HueSwing(1, 5, T / 7.0 + 3), 1.0, lockFactor) };
                        var stripB = new Color[] { Color.FromHsv(MathUtils.HueSwing(1, 5, T / 7.0 + 7), 1.0, lockFactor) };
                        z.Update(boardc.Concat(stripA).Concat(stripB).ToArray());
                    }
                    else
                    {
                        Color colourswing(int i) => Color.FromHsv(MathUtils.HueSwing(354, -20, T + i), 1.0, lockFactor);
                        Color[] colors = Enumerable.Range(0, (int)z.LedCount).Select(i => colourswing(i)).ToArray();
                        z.Update(colors);
                    }
                }

                foreach (Zone z in singleZones)
                {
                    Color[] colors = new Color[] { Color.FromHsv(MathUtils.HueSwing(357, 10, T / 7 + z.ID * 2.0), 1.0, 1.0) };
                    z.Update(colors);
                }

                foreach (var k in animators)
                {
                    k.Update(animUpdateState);
                }

                // Block until OpenRGB has processed all our updates, else it queues in TCP buffers asynchronously
                double checkpointTime = clock.ElapsedMilliseconds;
                client.GetControllerCount(); // Not if importance, just need something that respondes to block here

                double finTime = clock.ElapsedMilliseconds;
                double elapsed = Math.Max(0, finTime - startTime);
                int waitTime = (int)Math.Max(0, targetInterval - elapsed);
                Console.WriteLine("Waiting {0} ms ({1}ms compute, OpenRGB catch up {2}ms, target total {3})",
                    waitTime,
                    checkpointTime - startTime,
                    finTime - checkpointTime,
                    targetInterval);
                Thread.Sleep(waitTime);

            }
            return RunLightsResult.Done;
        }

        private static void StartForms()
        {
            SemaphoreSlim readyflag = new SemaphoreSlim(0, 1);

            Thread notifyThread = new Thread(
            delegate ()
            {
                SystemEvents.SessionSwitch += SessionSwitch;
                trayIcon = new NotifyIcon
                {
                    Text = "BMlights starting...",
                    Icon = new System.Drawing.Icon(Properties.Resources.trayicon, 40, 40)
                };

                ContextMenu trayMenu = new ContextMenu();

                trayMenu.MenuItems.Add("Reload", Reload);
                trayMenu.MenuItems.Add("Exit", Exit);

                trayIcon.ContextMenu = trayMenu;
                trayIcon.Visible = true;
                readyflag.Release(); // Everything initialised
                Application.Run(); // This runs forever to process events
            }
            );

            notifyThread.Start();
            readyflag.Wait();
        }

        private static void SessionSwitch(object sender, SessionSwitchEventArgs e)
        {
            if (e.Reason == SessionSwitchReason.SessionLock || e.Reason == SessionSwitchReason.SessionLogoff)
            {

                Console.WriteLine("Locked!");
                SessionLocked = true;
            }
            else if (e.Reason == SessionSwitchReason.SessionUnlock)
            {
                Console.WriteLine("Unlocked!");
                SessionLocked = false;
            }
        }

        private static void Reload(object sender, EventArgs e)
        {
            Console.WriteLine("Reloading");
            RunningLights = false;
            HoldingOnError = false;
        }
        private static void Exit(object sender, EventArgs e)
        {
            Console.WriteLine("Exiting");
            trayIcon.Dispose();
            SystemEvents.SessionSwitch -= SessionSwitch;
            Application.Exit();
            RunningLights = false;
            RunningApplication = false;
        }
    }
}
