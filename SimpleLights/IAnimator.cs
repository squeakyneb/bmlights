﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleLights
{
    public struct AnimUpdateState
    {
        public Double T;
        public bool locked;
    }

    public interface IAnimator
    {

        void Update(AnimUpdateState _state);
    }
}
