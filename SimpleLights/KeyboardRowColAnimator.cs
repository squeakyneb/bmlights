﻿using OpenRGB.NET.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleLights
{
    public class KeyboardRowColAnimator : IAnimator
    {
        Zone zone;
        enum AnimState { Rows, Cols }
        AnimState state;
        uint step;
        double lastStep = 0.0;

        public KeyboardRowColAnimator(Zone _zone)
        {
            if (_zone.MatrixMap == null)
            {
                throw new Exception("Can't do a matrix animation for a device with a null map!");
            }
            zone = _zone;
            state = AnimState.Rows;
            step = 0;
            lastStep = double.NegativeInfinity;
        }

        public void Update(AnimUpdateState _state)
        {
            if (_state.T - lastStep > .25)
            {
                lastStep = _state.T;
                step += 1;
                if (state == AnimState.Rows && step == zone.MatrixMap.Height)
                {
                    step = 0;
                    state = AnimState.Cols;
                }
                else if (state == AnimState.Cols && step == zone.MatrixMap.Width)
                {
                    step = 0;
                    state = AnimState.Rows;
                }
            }
            var leds = Enumerable.Range(0, (int)zone.LedCount).Select(i => new Color(0, 0, 0)).ToArray();
            switch (state)
            {
                case AnimState.Rows:
                    foreach (var i in Enumerable.Range(0, (int)zone.MatrixMap.Width))
                    {
                        var keycode = zone.MatrixMap.Matrix[step, i];
                        if (keycode < zone.LedCount)
                        {
                            leds[keycode] = new Color(255, 255, 255);
                        }
                    }
                    break;
                case AnimState.Cols:

                    foreach (var i in Enumerable.Range(0, (int)zone.MatrixMap.Height))
                    {
                        var keycode = zone.MatrixMap.Matrix[i, step];
                        if (keycode < zone.LedCount)
                        {
                            leds[keycode] = new Color(255, 255, 255);
                        }
                    }
                    break;
            }
            zone.Update(leds);
        }
    }
}
